package com.android.foodapp.model;

import java.io.Serializable;

public class Food implements Serializable {
    private String id;
    private String productName;

    // Quantity in 100g
    private double vitaminA;
    private double vitaminC;
    private double fat;
    private double proteins;
    private double carbohydrate;
    private double cholesterol;
    private double sugar;
    private double calcium;
    private double energy;
    private double fiber;
    private double iron;
    private double sodium;
    private double salt;


    private String ingredients;
    private String country;
    private String brands;
    private String url;
    private String image_url;

    private Double[] codeGeo;

    public Food(String id, String productName, double vitaminA, double vitaminC, double fat, double proteins, double carbohydrate, double cholesterol, double sugar, double calcium, double energy, double fiber, double iron, double sodium, double salt, String ingredients, String country, String brands, String url, String image_url, Double[] codeGeo) {
        this.id = id;
        this.productName = productName;
        this.vitaminA = vitaminA;
        this.vitaminC = vitaminC;
        this.fat = fat;
        this.proteins = proteins;
        this.carbohydrate = carbohydrate;
        this.cholesterol = cholesterol;
        this.sugar = sugar;
        this.calcium = calcium;
        this.energy = energy;
        this.fiber = fiber;
        this.iron = iron;
        this.sodium = sodium;
        this.salt = salt;
        this.ingredients = ingredients;
        this.country = country;
        this.brands = brands;
        this.url = url;
        this.image_url = image_url;
        this.codeGeo = codeGeo;
    }

    public String getId() {
        return id;
    }

    public String getProductName() {
        return productName;
    }

    public double getVitaminA() {
        return vitaminA;
    }

    public double getVitaminC() {
        return vitaminC;
    }

    public double getFat() {
        return fat;
    }

    public double getProteins() {
        return proteins;
    }

    public double getCarbohydrate() {
        return carbohydrate;
    }

    public double getCholesterol() {
        return cholesterol;
    }

    public double getSugar() {
        return sugar;
    }

    public double getCalcium() {
        return calcium;
    }

    public double getEnergy() {
        return energy;
    }

    public double getFiber() {
        return fiber;
    }

    public double getIron() {
        return iron;
    }

    public double getSodium() {
        return sodium;
    }

    public double getSalt() {
        return salt;
    }

    public String getIngredients() {
        return ingredients;
    }

    public String getCountry() {
        return country;
    }

    public String getBrands() {
        return brands;
    }

    public String getUrl() {
        return url;
    }

    public String getImage_url() {
        return image_url;
    }

    public Double[] getCodeGeo() {
        return codeGeo;
    }

    @Override
    public String toString() {
        return "Food{" +
                "productName=" + productName +
                ", vitaminA=" + vitaminA +
                ", vitaminC=" + vitaminC +
                ", fat=" + fat +
                ", proteins=" + proteins +
                ", carbohydrate=" + carbohydrate +
                ", cholesterol=" + cholesterol +
                ", sugar=" + sugar +
                ", calcium=" + calcium +
                ", energy=" + energy +
                ", fiber=" + fiber +
                ", iron=" + iron +
                ", sodium=" + sodium +
                ", salt=" + salt +
                ", ingredients='" + ingredients + '\'' +
                ", country='" + country + '\'' +
                ", brands='" + brands + '\'' +
                ", url='" + url + '\'' +
                '}';
    }
}
