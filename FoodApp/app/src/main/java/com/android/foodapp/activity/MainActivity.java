package com.android.foodapp.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.Toast;

import com.android.foodapp.adapter.MyAdapter;
import com.android.foodapp.R;
import com.android.foodapp.async.AsyncJSON;
import com.android.foodapp.model.Food;
import com.google.gson.Gson;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private boolean threadAlreadyLaunch = false;

    public static  final String _KEY_FROM_MAIN_ = "kMain";

    public static AsyncTask<Object,Integer,String> theard = null;

    public SearchView search;
    public ProgressBar progressBar;

    String searchString;

    Button btn_reset;
    Button btn_setting;

    public ArrayList<Food> foods = SplashScreen.launchArray;
    public static ArrayList<Food> favFoods = new ArrayList<>();

    public MyAdapter adapter;
    ListView lv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Restore fav list
        this.restore();

        // Init
        this.search = findViewById(R.id.searchView);
        search.setQuery("",false);
        searchString = "";
        this.progressBar = findViewById(R.id.progressBar);

        this.lv = findViewById(R.id.listView);
        this.adapter = new MyAdapter(
                foods,
                MainActivity.this
        );
        this.lv.setAdapter(adapter);

        this.search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                if(theard != null){
                    theard.cancel(true);
                }
                foods.clear();
                adapter.notifyDataSetChanged();
                searchString = s;
                AsyncJSON.start = 0;
                theard = new AsyncJSON().execute(
                        MainActivity.this,
                        foods,
                        adapter,
                        progressBar,
                        s
                );
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });

        // show data
        this.lv.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
                        Intent myIntent = new Intent(MainActivity.this, ProfileFood.class);
                        Gson gson = new Gson();
                        String myJson = gson.toJson(adapter.getArraylist().get(pos));
                        myIntent.putExtra("_KEY_", _KEY_FROM_MAIN_);
                        myIntent.putExtra(_KEY_FROM_MAIN_, myJson);
                        setResult(RESULT_OK, myIntent);
                        startActivity(myIntent);
                    }
                }
        );

        // add to favorite
        this.lv.setOnItemLongClickListener(
                new AdapterView.OnItemLongClickListener() {
                    @Override
                    public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                        if(!FoodComparator.ArrayAlreadyContainFood(favFoods,foods.get(i))){
                            favFoods.add(foods.get(i));
                            Toast.makeText(MainActivity.this,foods.get(i).getProductName()+" add to your favorites",Toast.LENGTH_LONG).show();
                        }
                        else{
                            favRemove(foods.get(i));
                            Toast.makeText(MainActivity.this,foods.get(i).getProductName()+" remove to your favorites",Toast.LENGTH_LONG).show();
                        }
                        save(MainActivity.this);
                        adapter.notifyDataSetChanged();
                        return false;
                    }
                }
        );

        this.lv.setOnScrollListener(new AbsListView.OnScrollListener() {
            private int currentVisibleItemCount;
            private int currentScrollState;
            private int currentFirstVisibleItem;
            private int totalItem;

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                // TODO Auto-generated method stub
                this.currentScrollState = scrollState;
                this.isScrollCompleted();

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {
                // TODO Auto-generated method stub
                this.currentFirstVisibleItem = firstVisibleItem;
                this.currentVisibleItemCount = visibleItemCount;
                this.totalItem = totalItemCount;

            }

            private void isScrollCompleted() {
                if (totalItem - currentFirstVisibleItem == currentVisibleItemCount
                        && this.currentScrollState == SCROLL_STATE_IDLE) {
                    if(!threadAlreadyLaunch){
                        threadAlreadyLaunch = true;
                        theard = new AsyncJSON().execute(
                                MainActivity.this,
                                foods,
                                adapter,
                                progressBar,
                                searchString
                        );
                        adapter.notifyDataSetChanged();
                        theard = null;
                        threadAlreadyLaunch = false;
                    }

                }
            }
        });

        this.btn_reset = findViewById(R.id.btn_reset);
        this.btn_reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetSearch();
            }
        });

        this.btn_setting = findViewById(R.id.btn_setting);
        this.btn_setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(MainActivity.this, PreferenceActivity.class);
                setResult(RESULT_OK,myIntent);
                startActivity(myIntent);
            }
        });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        this.save(MainActivity.this);
    }

    public static void save(Context context){
        FileOutputStream fos = null;
        ObjectOutputStream out = null;
        try {
            fos = context.openFileOutput("saveFile", Context.MODE_PRIVATE);
            out = new ObjectOutputStream(fos);
            out.writeObject(MainActivity.favFoods);
            out.close();
            fos.close();
            Log.d("succes","save------------------");
        } catch (IOException e) {
            e.printStackTrace();
            Log.d("error","save-----------------");
        }
    }

    public void restore(){
        File directory = getFilesDir();
        File file = new File(directory, "saveFile");
        if(file.exists()){
            FileInputStream fis = null;
            ObjectInputStream in = null;
            try {
                fis = openFileInput("saveFile");
                in = new ObjectInputStream(fis);
                MainActivity.favFoods = (ArrayList<Food>) in.readObject();
                in.close();
                fis.close();
                Log.d("succes","restore-----------------");
                Log.d("favFoods length",String.valueOf(MainActivity.favFoods.size()));
            } catch (Exception e) {
                e.printStackTrace();
                Log.d("error","restore-----------------");
            }
        }
    }

    public static class FoodComparator{
        public static boolean compare(Food obj1, Food obj2){
            if(obj1.getClass() == obj2.getClass()){
                return obj1.getId().equals(obj2.getId());
            }
            return false;
        }

        public static boolean ArrayAlreadyContainFood(ArrayList<Food> array, Food food){
            for (Food f : array){
                if(compare(f,food)){
                    return true;
                }
            }
            return false;
        }
    }

    public static boolean favRemove(Food obj){
        for (Food f : MainActivity.favFoods){
            if(f.getId().equals(obj.getId())){
                MainActivity.favFoods.remove(f);
                return true;
            }
        }
        return false;
    }

    public void resetSearch(){
        this.search.clearFocus();
        this.search.setQuery("",false);
        this.searchString = "";
        if(theard != null){
            theard.cancel(true);
        }
        foods.clear();
        adapter.notifyDataSetChanged();
        AsyncJSON.start = 0;
        theard = new AsyncJSON().execute(
                MainActivity.this,
                foods,
                adapter,
                progressBar,
                searchString
        );
    }

    @Override
    protected void onStart() {
        super.onStart();
        this.adapter.notifyDataSetChanged();
    }
}