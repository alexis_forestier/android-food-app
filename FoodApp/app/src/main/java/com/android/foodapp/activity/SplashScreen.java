package com.android.foodapp.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import com.android.foodapp.R;
import com.android.foodapp.async.AsyncJSON;
import com.android.foodapp.model.Food;
import com.android.foodapp.utility.Const;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

@SuppressLint("CustomSplashScreen")
public class SplashScreen extends AppCompatActivity {

    private static int SPLASH_TIME_OUT = 3000;
    public static ArrayList<Food> launchArray = new ArrayList<>();

    private SharedPreferences sharedPreferences;
    boolean darkModeBool;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        sharedPreferences = this.getSharedPreferences("appPreference", Context.MODE_PRIVATE);
        darkModeBool = sharedPreferences.getBoolean("darkMode",false);
        this.toggleDarkMode();

        if(!AsyncJSON.CheckInternetConnection(SplashScreen.this)){
            // No connection
            Toast.makeText(SplashScreen.this, "no internet connection", Toast.LENGTH_SHORT).show();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    while(!AsyncJSON.CheckInternetConnection(SplashScreen.this)){
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    new AsyncLaunch().execute();
                }
            }, SPLASH_TIME_OUT);
        }
        else {
            new AsyncLaunch().execute();
        }
    }

    private class AsyncLaunch extends AsyncTask<Object,Integer,String> {

        @Override
        protected String doInBackground(Object... objects) {
            String result = "";

            String message = AsyncJSON.getAPIData("https://public.opendatasoft.com/api/records/1.0/search/?dataset=open-food-facts-products&q=&rows="+20+"&start="+0+"&facet=creator&facet=created_datetime&facet=packaging_tags&facet=brands_tags&facet=categories_tags&facet=categories_fr&facet=origins_tags&facet=manufacturing_places_tags&facet=labels_tags&facet=labels_fr&facet=cities_tags&facet=countries_tags&facet=allergens&facet=traces_tags&facet=ingredients_from_palm_oil_n&facet=ingredients_that_may_be_from_palm_oil_n&facet=nutrition_grade_fr&facet=main_category&facet=energy_100g&facet=fat_100g&facet=sugars_100g&timezone=Europe%2FParis");

            try {
                JSONObject data = new JSONObject(message);
                JSONArray jsonArray = data.getJSONArray("records");

                for (int i = 0; i < jsonArray.length();i++){
                    JSONObject field = jsonArray.getJSONObject(i).getJSONObject("fields");
                    JSONArray arrayGeo = field.optJSONArray("first_packaging_code_geo");
                    Double[] doubleArray = new Double[0];
                    if(arrayGeo != null){
                        doubleArray = new Double[2];
                        doubleArray[0] = arrayGeo.optDouble(0);
                        doubleArray[1] = arrayGeo.optDouble(1);
                    }

                    if(!field.optString("product_name", "").equals("")){
                        SplashScreen.launchArray.add(new Food(
                                jsonArray.getJSONObject(i).optString("recordid"),
                                field.optString("product_name","no data"),
                                field.optDouble("vitamin_a_100g",0),
                                field.optDouble("vitamin_c_100g",0),
                                field.optDouble("fat_100g",0),
                                field.optDouble("proteins_100g",0),
                                field.optDouble("carbohydrates_100g",0),
                                field.optDouble("cholesterol_100g",0),
                                field.optDouble("sugars_100g",0),
                                field.optDouble("calcium_100g",0),
                                field.optDouble("energy_100g",0),
                                field.optDouble("fiber_100g",0),
                                field.optDouble("iron_100g",0),
                                field.optDouble("sodium_100g",0),
                                field.optDouble("salt_100g",0),
                                field.optString("ingredients_text","no data"),
                                field.optString("countries","no data"),
                                field.optString("brands","no data"),
                                field.optString("url","no data"),
                                field.optString("image_url","no data"),
                                doubleArray
                        ));
                    }
                }

                Log.d("json","success");
                return "";
            } catch (JSONException e) {
                e.printStackTrace();
                Log.d("json","failed");
                return "error";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if(!Const._MAIN_ALREADY_LAUNCH_){
                Intent i = new Intent(SplashScreen.this, MainActivity.class);
                startActivity(i);

                Const._MAIN_ALREADY_LAUNCH_ = true;
                // close this activity
                finish();
            }

        }
    }

    public void toggleDarkMode() {
        if(darkModeBool) {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        }
        else {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }
    }




}