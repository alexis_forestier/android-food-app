package com.android.foodapp.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.android.foodapp.R;
import com.android.foodapp.activity.MainActivity;
import com.android.foodapp.model.Food;

import java.util.ArrayList;

public class MyAdapter extends BaseAdapter {
    ArrayList<Food> arraylist;
    Context context;
    boolean darkModeBool;

    public MyAdapter(ArrayList<Food> arraylist, Context context) {
        this.arraylist = arraylist;
        this.context = context;
        SharedPreferences sharedPreferences = this.context.getSharedPreferences("appPreference", Context.MODE_PRIVATE);
        this.darkModeBool = sharedPreferences.getBoolean("darkMode",false);

    }

    public ArrayList<Food> getArraylist() {
        return arraylist;
    }

    @Override
    public int getCount() {
        return arraylist.size();
    }

    @Override
    public Object getItem(int i) {
        return arraylist.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        ConstraintLayout layoutItem;
        LayoutInflater mInflater = LayoutInflater.from(context);

        //(1) : Réutilisation du layout
        if (convertView == null) {
            layoutItem = (ConstraintLayout) mInflater.inflate(R.layout.item_layout, viewGroup, false);
        } else {
            layoutItem = (ConstraintLayout) convertView;
        }

        //(2) : Récupération des TextView de notre layout
        TextView tv = layoutItem.findViewById(R.id.item);
        ImageView maps = layoutItem.findViewById(R.id.mapsIcon);
        ImageView image = layoutItem.findViewById(R.id.imageIcon);
        LinearLayout ll = layoutItem.findViewById(R.id.layoutAdapter);
        //(3) : Mise à jour des valeurs
        tv.setText(arraylist.get(i).getProductName());

        //Map icon
        if(arraylist.get(i).getCodeGeo().length < 2){
            maps.setVisibility(View.INVISIBLE);
        }else{
            maps.setVisibility(View.VISIBLE);
        }

        //Image icon
        if(arraylist.get(i).getImage_url().equals("no data")){
           image.setVisibility(View.INVISIBLE);
        }
        else{
            image.setVisibility(View.VISIBLE);
        }

        //Fav color
        if(MainActivity.FoodComparator.ArrayAlreadyContainFood(MainActivity.favFoods,arraylist.get(i))){
            ll.setBackgroundColor(Color.YELLOW);
            tv.setTextColor(Color.BLACK);
        }
        else{
            ll.setBackgroundColor(Color.argb(0,0,0,0));
            if(this.darkModeBool){
                tv.setTextColor(Color.WHITE);
            }
        }


        //On retourne l'item créé.
        return layoutItem;
    }

}
