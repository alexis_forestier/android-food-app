package com.android.foodapp.async;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;
import android.widget.ProgressBar;

import androidx.annotation.RequiresApi;

import com.android.foodapp.model.Food;
import com.android.foodapp.adapter.MyAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

@SuppressLint("StaticFieldLeak")
public class AsyncJSON extends AsyncTask<Object,Integer,String> {

    public static int row = 20;
    public static int start = 20;

    Context context;
    ArrayList<Food> foods;
    MyAdapter adapter;
    ProgressBar progress;
    String search;

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected String doInBackground(Object... objects) {

        context = (Context) objects[0];
        foods = (ArrayList<Food>) objects[1];
        adapter = (MyAdapter) objects[2];
        progress = (ProgressBar) objects[3];
        search = (String) objects[4];


        String result = "";
        if(AsyncJSON.CheckInternetConnection(context)){
            result = getAPIResult();
        }

        return result;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onPostExecute(String result){
        Log.d("foods size",String.valueOf(foods.size()));
        Log.d("foods",foods.toString());
        Log.d("start",String.valueOf(start));
        start += row;

        adapter.notifyDataSetChanged();
        progress.setProgress(100,true);
    }

    public String getSearch(){
        if(!this.search.equals("")){
            return "product_name%3D"+this.search;
        }
        return "";
    }

    public String getURL(){
        return "https://public.opendatasoft.com/api/records/1.0/search/?dataset=open-food-facts-products&q="+getSearch()+"&rows="+this.row+"&start="+this.start+"&facet=creator&facet=created_datetime&facet=packaging_tags&facet=brands_tags&facet=categories_tags&facet=categories_fr&facet=origins_tags&facet=manufacturing_places_tags&facet=labels_tags&facet=labels_fr&facet=cities_tags&facet=countries_tags&facet=allergens&facet=traces_tags&facet=ingredients_from_palm_oil_n&facet=ingredients_that_may_be_from_palm_oil_n&facet=nutrition_grade_fr&facet=main_category&facet=energy_100g&facet=fat_100g&facet=sugars_100g&timezone=Europe%2FParis";
    }

    public static String getAPIData(String string_url){
        String message = "";
        try{
            URL url = new URL(string_url);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            if (urlConnection.getResponseCode() == HttpURLConnection.HTTP_OK){
                BufferedReader in = new BufferedReader(
                        new InputStreamReader(urlConnection.getInputStream()));
                message = in.readLine();
                in.close();
            }
            urlConnection.disconnect();
        }catch (Exception e){
            e.printStackTrace();
        }
        return message;
    }

    public String jsonParsing(String json){
        try {
            JSONObject data = new JSONObject(json);
            JSONArray jsonArray = data.getJSONArray("records");

            for (int i = 0; i < jsonArray.length();i++){
                JSONObject field = jsonArray.getJSONObject(i).getJSONObject("fields");
                JSONArray arrayGeo = field.optJSONArray("first_packaging_code_geo");
                Double[] doubleArray = new Double[0];
                if(arrayGeo != null){
                    doubleArray = new Double[2];
                    doubleArray[0] = arrayGeo.optDouble(0);
                    doubleArray[1] = arrayGeo.optDouble(1);
                }

                if(!field.optString("product_name", "").equals("")){
                    foods.add(new Food(
                            jsonArray.getJSONObject(i).optString("recordid"),
                            field.optString("product_name","no data"),
                            field.optDouble("vitamin_a_100g",0),
                            field.optDouble("vitamin_c_100g",0),
                            field.optDouble("fat_100g",0),
                            field.optDouble("proteins_100g",0),
                            field.optDouble("carbohydrates_100g",0),
                            field.optDouble("cholesterol_100g",0),
                            field.optDouble("sugars_100g",0),
                            field.optDouble("calcium_100g",0),
                            field.optDouble("energy_100g",0),
                            field.optDouble("fiber_100g",0),
                            field.optDouble("iron_100g",0),
                            field.optDouble("sodium_100g",0),
                            field.optDouble("salt_100g",0),
                            field.optString("ingredients_text","no data"),
                            field.optString("countries","no data"),
                            field.optString("brands","no data"),
                            field.optString("url","no data"),
                            field.optString("image_url","no data"),
                            doubleArray
                    ));
                }
            }

            Log.d("json","success");
            return "";
        } catch (JSONException e) {
            e.printStackTrace();
            Log.d("json","failed");
            return "error";
        }

    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public String getAPIResult(){

        progress.setProgress(0,false);

        String message = getAPIData(getURL());
        Log.d("message",message);

        return jsonParsing(message);
    }

    public static boolean CheckInternetConnection(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isAvailable() && cm.getActiveNetworkInfo().isConnected();
    }
}
