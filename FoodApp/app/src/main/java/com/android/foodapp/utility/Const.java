package com.android.foodapp.utility;

public class Const {
    // to fix "the double main activity launch bug"
    public static boolean _MAIN_ALREADY_LAUNCH_ = false;
}
