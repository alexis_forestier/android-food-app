package com.android.foodapp.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.foodapp.R;
import com.android.foodapp.async.DownloadImageTask;
import com.android.foodapp.model.Food;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;

public class ProfileFood extends AppCompatActivity implements OnMapReadyCallback {

    Food food;
    private GoogleMap gmap;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_food);

        //init
        TextView productName = findViewById(R.id.txt_productname);
        TextView vitaminA = findViewById(R.id.txt_vitA);
        TextView vitaminC = findViewById(R.id.txt_vitC);
        TextView fat = findViewById(R.id.txt_fat);
        TextView proteins = findViewById(R.id.txt_proteins);
        TextView carbohydrate = findViewById(R.id.txt_carbohydrate);
        TextView cholesterol = findViewById(R.id.txt_cholesterol);
        TextView sugar = findViewById(R.id.txt_sugar);
        TextView calcium = findViewById(R.id.txt_calcium);
        TextView energy = findViewById(R.id.txt_energy);
        TextView fiber = findViewById(R.id.txt_fiber);
        TextView iron = findViewById(R.id.txt_iron);
        TextView sodium = findViewById(R.id.txt_sodium);
        TextView salt = findViewById(R.id.txt_salt);

        TextView ingredients = findViewById(R.id.txt_ingredients);
        TextView country = findViewById(R.id.txt_country);
        TextView brands = findViewById(R.id.txt_brands);
        TextView geoCode = findViewById(R.id.txt_geoCode);
        TextView url = findViewById(R.id.txt_url);
        ImageView image_url = findViewById(R.id.imageProd);

        Gson gson = new Gson();
        String key = getIntent().getStringExtra("_KEY_");
        this.food = gson.fromJson(getIntent().getStringExtra(key),Food.class);

        //set
        productName.setText(this.food.getProductName());
        vitaminA.setText(Double.toString(this.food.getVitaminA()));
        vitaminC.setText(Double.toString(this.food.getVitaminC()));
        fat.setText(Double.toString(this.food.getFat()));
        proteins.setText(Double.toString(this.food.getProteins()));
        carbohydrate.setText(Double.toString(this.food.getCarbohydrate()));
        cholesterol.setText(Double.toString(this.food.getCholesterol()));
        sugar.setText(Double.toString(this.food.getSugar()));
        calcium.setText(Double.toString(this.food.getCalcium()));
        energy.setText(Double.toString(this.food.getEnergy()));
        fiber.setText(Double.toString(this.food.getFiber()));
        iron.setText(Double.toString(this.food.getIron()));
        sodium.setText(Double.toString(this.food.getSodium()));
        salt.setText(Double.toString(this.food.getSalt()));

        ingredients.setText(this.food.getIngredients());
        country.setText(this.food.getCountry());
        brands.setText(this.food.getBrands());
        if(this.food.getCodeGeo().length == 2){
            geoCode.setText("["+this.food.getCodeGeo()[0]+", "+this.food.getCodeGeo()[1]+"]");
        }
        else {
            geoCode.setText("no data");
        }
        url.setText(this.food.getUrl());

        if(!this.food.getImage_url().equals("no data")){
            new DownloadImageTask(image_url).execute(this.food.getImage_url());
        }

        if(this.food.getCodeGeo().length == 2){
            // Obtain the SupportMapFragment and get notified when the map is ready to be used.
            SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.mapProfile);
            mapFragment.getMapAsync(this);
        }

    }

    @Override
    public void onBackPressed() {
        Intent myIntent = new Intent(ProfileFood.this, MainActivity.class);
        setResult(RESULT_OK, myIntent);
        super.onBackPressed();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        gmap = googleMap;

        // Add a marker and move the camera
        LatLng foodProduct = new LatLng(this.food.getCodeGeo()[0],this.food.getCodeGeo()[1]);
        gmap.addMarker(new MarkerOptions().position(foodProduct).title(this.food.getProductName()));
        gmap.moveCamera(CameraUpdateFactory.newLatLng(foodProduct));
    }

}