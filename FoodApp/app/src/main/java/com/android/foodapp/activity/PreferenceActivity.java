package com.android.foodapp.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.android.foodapp.adapter.MyAdapter;
import com.android.foodapp.R;
import com.google.gson.Gson;

public class PreferenceActivity extends AppCompatActivity {

    public static  final String _KEY_FROM_PREF_ = "kPref";

    Switch darkModeSwitch;
    Button btnSave;

    public MyAdapter adapter;
    ListView lv;

    TextView txt_fav;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preference);

        txt_fav = findViewById(R.id.txt_fav);
        txt_fav.setText(new String( "Favorites Food List - " + MainActivity.favFoods.size()));

        SharedPreferences sharedPreferences = this.getSharedPreferences("appPreference", Context.MODE_PRIVATE);
        boolean darkModeBool = sharedPreferences.getBoolean("darkMode",false);
        this.darkModeSwitch = findViewById(R.id.switch_dark);
        this.darkModeSwitch.setChecked(darkModeBool);
        this.darkModeSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(darkModeSwitch.isChecked()) {
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
                }
                else {
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
                }

            }
        });

        this.btnSave = this.findViewById(R.id.btn_save);
        this.btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                savePreference();
            }
        });



        this.lv = findViewById(R.id.listViewFav);
        this.adapter = new MyAdapter(
                MainActivity.favFoods,
                PreferenceActivity.this
        );
        this.lv.setAdapter(adapter);

        this.lv.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
                        Intent myIntent = new Intent(PreferenceActivity.this, ProfileFood.class);
                        Gson gson = new Gson();
                        String myJson = gson.toJson(adapter.getArraylist().get(pos));
                        myIntent.putExtra("_KEY_", _KEY_FROM_PREF_);
                        myIntent.putExtra(_KEY_FROM_PREF_, myJson);
                        setResult(RESULT_OK, myIntent);
                        startActivity(myIntent);
                    }
                }
        );

        // add to favorite
        this.lv.setOnItemLongClickListener(
                new AdapterView.OnItemLongClickListener() {
                    @Override
                    public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {

                        Toast.makeText(PreferenceActivity.this,MainActivity.favFoods.get(i).getProductName()+" remove to your favorites",Toast.LENGTH_LONG).show();
                        MainActivity.favFoods.remove(i);

                        MainActivity.save(PreferenceActivity.this);
                        adapter.notifyDataSetChanged();
                        txt_fav.setText(new String("Favorites Food List - " + MainActivity.favFoods.size()));
                        return false;
                    }
                }
        );

    }

    public void savePreference()  {
        SharedPreferences sharedPreferences= this.getSharedPreferences("appPreference", Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putBoolean("darkMode", this.darkModeSwitch.isChecked());

        editor.apply();

        Toast.makeText(this,"Preference saved",Toast.LENGTH_SHORT).show();
    }

}